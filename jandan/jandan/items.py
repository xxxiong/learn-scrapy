# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class JandanItem(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field()      # 标题
    author = scrapy.Field()     # 作者
    link = scrapy.Field()       # 正文链接地址
    summary = scrapy.Field()    # 文章摘要

