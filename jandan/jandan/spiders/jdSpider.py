# -*- coding:utf8 -*-
import scrapy
from jandan.items import JandanItem


class jiandSpider(scrapy.Spider):
    name = 'jand'
    start_urls = [
        'file:///home/xiong/learn-scrapy/web/20.html',
    ]
    def parse(self, response):
        for sel in response.xpath('//*[@id="content"]/div'):
            link = response.urljoin(sel.xpath('div/h2/a/@href').extract()[0])
            yield scrapy.Request(link, callback=self.parse_link_contents)

    def parse_link_contents(self, response):
        item = JandanItem()
        item['title'] = response.xpath('//*[@id="content"]/div[2]/h1/a/text()').extract()
        item['author'] = response.xpath('//*[@id="content"]/div[2]/div[1]/a/text()').extract()
        item['link'] = response.url
        item['summary'] = response.xpath('//*[@id="content"]/div[2]/p[1]/img/@alt').extract()
        yield item
