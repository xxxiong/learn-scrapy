## [原文地址](https://scrapy-chs.readthedocs.io/zh_CN/1.0/intro/tutorial.html#id2)

## 1.1.4 保存爬取到的数据
最简单的存储爬取的数据的方式:
```
scrapy crawl jand -o items.json
```
该目录将采用 JSON 格式对爬取的数据进行序列化, 生成`items.json`文件.


## 1.1.3 追踪链接
接下来, 不仅仅满足于爬取 列表页, 你想要获取正文页的内容.

既然已经能从页面上爬取数据了, 为什么不提取你感兴趣的链接追踪他们, 读取这些链接的数据呢?

下面这个是实现这个功能的改进版spider:
```
# -*- coding:utf8 -*-
import scrapy
from jandan.items import JandanItem


class jiandSpider(scrapy.Spider):
    name = 'jand'
    start_urls = [
        'file:///home/xiong/learn-scrapy/web/20.html',
    ]
    def parse(self, response):
        for sel in response.xpath('//*[@id="content"]/div'):
            link = response.urljoin(sel.xpath('div/h2/a/@href').extract()[0])
            yield scrapy.Request(link, callback=self.parse_link_contents)

    def parse_link_contents(self, response):
        item = JandanItem()
        item['title'] = response.xpath('//*[@id="content"]/div[2]/h1/a/text()').extract()
        item['author'] = response.xpath('//*[@id="content"]/div[2]/div[1]/a/text()').extract()
        item['link'] = response.url
        item['summary'] = response.xpath('//*[@id="content"]/div[2]/p[1]/img/@alt').extract()
        yield item
```
现在, `parse()`仅仅从页面中提取我们感兴趣的链接, 使用`response.urljoin()`方法构造一个绝对路劲URL, 产生(yield)一个请求, 该请求使用`parse_link_contents()`方法作为回调函数, 用于最终产生我们想要的数据.

这里展现的即是Scrapy的追踪机制: 当你在回调函数中yield一个Request后, Scrapy将会调度, 发送该请求, 并且在该请求完成时, 调用所注册的回调函数.

基于此方法, 你可以更具所东一的跟进连接的规则, 创建负责的crawler, 并且, 根据所访问的页面, 提取不同的数据.

一种常见的方法是, 回调函数负责提取一些item, 查找能跟进的页面链接, 并且使用相同回调函数yield一个Request:
```
def parse_articles_follow_next_page(self, response):
    for article in response.xpath("//article"):
        item = ArticleItem()

        ... extract article data here

        yield item

    next_page = response.css("ul.navigation > li.next-page > a::attr('href')")
    if next_page:
        url = response.urljoin(next_page[0].extract())
        yield scrapy.Request(url, self.parse_articles_follow_next_page)
```
上述代码将创建一个循环, 跟进所有下一页的链接, 知道找不到为止 -- 对于爬取博客, 论坛以及其他做了分页的网站十分有效.

另一种常见的需求是从多个页面构建item的数据, [在回调函数中传递信息的技巧](#) 会有提到.


## 1.1.2 提取Item
#### 定义Item
Item是保存爬取到的数据的容器; 其使用方法和Python字典类似. 虽然您也可以在Scrapy中直接使用dict, 但是 Item 提供了额外保护机制来避免拼写错误导致的未定义字段错误.

类似在ORM中做的一样, 您可以通过创建一个 `scrapy.Item` 类,  并且定义类型为 `scrapy.Field` 的类属性来定义一个Item. (如果不了解ORM, 不用担心, 您会发现这个步骤非常简单)

首先根据需要从目标网站获取到的数据对item进行建模。 我们需要从`spider`(spidername)中获取名字, url, 以及网站的描述. 对此, 在item中定义相应的字段. 编辑 `jandan` 目录中的 `items.py` 文件:
```
import scrapy

class jiandanItem(scrapy.Item):
    title = scrapy.Field()      # 标题
    author = scrapy.Field()     # 作者
    link = scrapy.Field()       # 正文链接地址
    summary = scrapy.Field()    # 文章摘要
```
一开始这看起来可能有点复杂, 但是通过定义item, 你可以很方便的使用scrapy的其他方法. 而这些方法需要你的item定义.

#### 提取Item
###### selectors 选择器简介
从网页中提取数据有很多方法. Scrapy使用了一种基于`XPATH`和`CSS`表达式机制: `Scrapy Selectors`
- 关于selector和其他提取机制的信息参考: [Selector文档](https://scrapy-chs.readthedocs.io/zh_CN/1.0/topics/selectors.html#topics-selectors)

这里给出XPATH表达式的例子及对应的含义:
- `/html/head/title`        : 选择HTML文档中`<head>`标签`<title>`元素;
- `/html/head/title/text()` : 选择上面提到的`<title>`元素的文字;
- `//td`                    : 选择所有`<td>`元素;
- `//div[@class="mine"]`    : 算则所有具有`class="mine"`属性的`div`元素
- ...
为了配合CSS与Xpath, Scrapy除了提供`Selector`之外, 还提供了方法来避免每次从response中提取数据是生成selector的麻烦.

Selector 有四个基本的方法:
- `xpath`     : 传入xpath表达式, 返回该表达式所对应的所有节点的selector列表;
- `css()`     : 传入CSS表达式, 返回该表达式锁对应的所有节点的selector列表;
- `extract()` : 序列化该节点为unicode字符串并返回list;
- `re()`      : 根据传入的正则表达式对数据进行提取, 返回unicode字符串列表.

###### 在shell中尝试Selector选择器
需要进入项目的根目录, 执行下列命令来启动shell:
```
[xiong@localhost ~]$ scrapy shell file:///home/xiong/learn-scrapy/web/20.html
2018-06-30 10:45:20 [scrapy.utils.log] INFO: Scrapy 1.5.0 started (bot: scrapybot)
2018-06-30 10:45:20 [scrapy.utils.log] INFO: Versions: lxml 4.2.2.0, libxml2 2.9.8, cssselect 1.0.3, parsel 1.4.0, w3lib 1.19.0, Twisted 18.4.0, Python 3.7.0a3 (default, Apr 22 2018, 16:54:46) - [GCC 4.4.7 20120313 (Red Hat 4.4.7-18)], pyOpenSSL 18.0.0 (OpenSSL 1.1.0h  27 Mar 2018), cryptography 2.2.2, Platform Linux-2.6.32-573.el6.x86_64-x86_64-with-centos-6.7-Final
2018-06-30 10:45:20 [scrapy.crawler] INFO: Overridden settings: {'DUPEFILTER_CLASS': 'scrapy.dupefilters.BaseDupeFilter', 'LOGSTATS_INTERVAL': 0}
2018-06-30 10:45:20 [scrapy.middleware] INFO: Enabled extensions:
['scrapy.extensions.corestats.CoreStats',
 'scrapy.extensions.telnet.TelnetConsole',
 'scrapy.extensions.memusage.MemoryUsage']
2018-06-30 10:45:20 [scrapy.middleware] INFO: Enabled downloader middlewares:
['scrapy.downloadermiddlewares.httpauth.HttpAuthMiddleware',
 'scrapy.downloadermiddlewares.downloadtimeout.DownloadTimeoutMiddleware',
 'scrapy.downloadermiddlewares.defaultheaders.DefaultHeadersMiddleware',
 'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware',
 'scrapy.downloadermiddlewares.retry.RetryMiddleware',
 'scrapy.downloadermiddlewares.redirect.MetaRefreshMiddleware',
 'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware',
 'scrapy.downloadermiddlewares.redirect.RedirectMiddleware',
 'scrapy.downloadermiddlewares.cookies.CookiesMiddleware',
 'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware',
 'scrapy.downloadermiddlewares.stats.DownloaderStats']
2018-06-30 10:45:20 [scrapy.middleware] INFO: Enabled spider middlewares:
['scrapy.spidermiddlewares.httperror.HttpErrorMiddleware',
 'scrapy.spidermiddlewares.offsite.OffsiteMiddleware',
 'scrapy.spidermiddlewares.referer.RefererMiddleware',
 'scrapy.spidermiddlewares.urllength.UrlLengthMiddleware',
 'scrapy.spidermiddlewares.depth.DepthMiddleware']
2018-06-30 10:45:20 [scrapy.middleware] INFO: Enabled item pipelines:
[]
2018-06-30 10:45:20 [scrapy.extensions.telnet] DEBUG: Telnet console listening on 127.0.0.1:6024
2018-06-30 10:45:20 [scrapy.core.engine] INFO: Spider opened
2018-06-30 10:45:20 [scrapy.core.engine] DEBUG: Crawled (200) <GET file:///home/xiong/learn-scrapy/web/20.html> (referer: None)
[s] Available Scrapy objects:
[s]   scrapy     scrapy module (contains scrapy.Request, scrapy.Selector, etc)
[s]   crawler    <scrapy.crawler.Crawler object at 0x7f3c4a3c65c0>
[s]   item       {}
[s]   request    <GET file:///home/xiong/learn-scrapy/web/20.html>
[s]   response   <200 file:///home/xiong/learn-scrapy/web/20.html>
[s]   settings   <scrapy.settings.Settings object at 0x7f3c41b9e1d0>
[s]   spider     <DefaultSpider 'default' at 0x7f3c41b22208>
[s] Useful shortcuts:
[s]   fetch(url[, redirect=True]) Fetch URL and update local objects (by default, redirects are followed)
[s]   fetch(req)                  Fetch a scrapy.Request and update local objects 
[s]   shelp()           Shell help (print this help)
[s]   view(response)    View response in a browser
>>> 
```
当shell载入后, 你将得到一个包含response数据的本地`response`变量. 输入`response.body`将输出response的包体, 输出`response.headers`可以看到response的包头.

更为重要的是, `response`拥有一个`selector`属性, 该属性是以该特定`response`初始化的类`selector`的对象. 你可以通>过使用`response.selector.xpath()`或`response.selector.css()`来对`response`进行查询. 此外, scrapy也对`response.selector.xpath()`及`response.selector.css()`提供了一些快捷方式. 例如:`response.xpath()`或`response.css()`.

同时, shell根据response提前初始化了变量`se1`. 该selector根据response的类型自动选择最合适的分析规则(XML vs HTML)

让我们来试试:
```
>>> response.xpath('//title')
[<Selector xpath='//title' data='<title>\r\n2018年06月20日 - 煎蛋\r\n</title>'>]
>>> response.xpath('//title').extract()
['<title>\r\n2018年06月20日 - 煎蛋\r\n</title>']
>>> response.xpath('//title/text()')
[<Selector xpath='//title/text()' data='\r\n2018年06月20日 - 煎蛋\r\n'>]
>>> response.xpath('//title/text()').extract()
['\r\n2018年06月20日 - 煎蛋\r\n']
>>> response.xpath('//title/text()').re('(\w+)')
['2018年06月20日', '煎蛋']
```
###### 提取数据
现在我们来尝试从这些数据中提取些有用的数据.

在我们的spider中加入这段代码:
```
# -*- coding:utf8 -*-
import scrapy


class jiandSpider(scrapy.Spider):
    name = 'jand'
    start_urls = [
        'file:///home/xiong/learn-scrapy/web/20.html',
    ]
    def parse(self, response):
        for sel in response.xpath('//*[@id="content"]'):
            title = sel.xpath('div/div/h2/a/text()').extract()
            author = sel.xpath('div/div/div/a/text()').extract()
            link = sel.xpath('div/div/h2/a/@href').extract()
            summary = response.xpath('div/div/text()').extract()
            print(title, author, link, summary)
```
现在尝试再次启动爬虫...

###### 使用item
`Item`对象是自定义的Python字典. 你可以使用标准的字典语法来获取到其每个字段的值. (字段即是我们之前用Filed的属性)
```
>>> from jandan.items import JandanItem
>>> item = JandanItem()
>>> item['title'] = 'Example title'
>>> item['title']
'Example title'
```
为了将爬取的数据返回, 我们最终的代码将是:
```
# -*- coding:utf8 -*-
import scrapy
from jandan.items import JandanItem


class jiandSpider(scrapy.Spider):
    name = 'jand'
    start_urls = [
        'file:///home/xiong/learn-scrapy/web/20.html',
    ]
    def parse(self, response):
        for sel in response.xpath('//*[@id="content"]/div'):
            item = JandanItem()
            item['title'] = sel.xpath('div/h2/a/text()').extract()
            item['author'] = sel.xpath('div/div/a/text()').extract()
            item['link'] = response.urljoin(sel.xpath('div/h2/a/@href').extract()[0])
            item['summary'] = sel.xpath('div/text()').extract()
            yield item
```
现在再次进行爬取, 将会产生`JandanItem`对象:
```
[xiong@localhost jandan]$  scrapy crawl jand
2018-07-03 10:59:04 [scrapy.utils.log] INFO: Scrapy 1.5.0 started (bot: jandan)
2018-07-03 10:59:04 [scrapy.utils.log] INFO: Versions: lxml 4.2.2.0, libxml2 2.9.8, cssselect 1.0.3, parsel 1.4.0, w3lib 1.19.0, Twisted 18.4.0, Python 3.7.0a3 (default, Apr 22 2018, 16:54:46) - [GCC 4.4.7 20120313 (Red Hat 4.4.7-18)], pyOpenSSL 18.0.0 (OpenSSL 1.1.0h  27 Mar 2018), cryptography 2.2.2, Platform Linux-2.6.32-573.el6.x86_64-x86_64-with-centos-6.7-Final
2018-07-03 10:59:04 [scrapy.crawler] INFO: Overridden settings: {'BOT_NAME': 'jandan', 'NEWSPIDER_MODULE': 'jandan.spiders', 'ROBOTSTXT_OBEY': True, 'SPIDER_MODULES': ['jandan.spiders']}
2018-07-03 10:59:04 [scrapy.middleware] INFO: Enabled extensions:
['scrapy.extensions.corestats.CoreStats',
 'scrapy.extensions.telnet.TelnetConsole',
 'scrapy.extensions.memusage.MemoryUsage',
 'scrapy.extensions.logstats.LogStats']
2018-07-03 10:59:04 [scrapy.middleware] INFO: Enabled downloader middlewares:
['scrapy.downloadermiddlewares.robotstxt.RobotsTxtMiddleware',
 'scrapy.downloadermiddlewares.httpauth.HttpAuthMiddleware',
 'scrapy.downloadermiddlewares.downloadtimeout.DownloadTimeoutMiddleware',
 'scrapy.downloadermiddlewares.defaultheaders.DefaultHeadersMiddleware',
 'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware',
 'scrapy.downloadermiddlewares.retry.RetryMiddleware',
 'scrapy.downloadermiddlewares.redirect.MetaRefreshMiddleware',
 'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware',
 'scrapy.downloadermiddlewares.redirect.RedirectMiddleware',
 'scrapy.downloadermiddlewares.cookies.CookiesMiddleware',
 'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware',
 'scrapy.downloadermiddlewares.stats.DownloaderStats']
2018-07-03 10:59:04 [scrapy.middleware] INFO: Enabled spider middlewares:
['scrapy.spidermiddlewares.httperror.HttpErrorMiddleware',
 'scrapy.spidermiddlewares.offsite.OffsiteMiddleware',
 'scrapy.spidermiddlewares.referer.RefererMiddleware',
 'scrapy.spidermiddlewares.urllength.UrlLengthMiddleware',
 'scrapy.spidermiddlewares.depth.DepthMiddleware']
2018-07-03 10:59:04 [scrapy.middleware] INFO: Enabled item pipelines:
[]
2018-07-03 10:59:04 [scrapy.core.engine] INFO: Spider opened
2018-07-03 10:59:04 [scrapy.extensions.logstats] INFO: Crawled 0 pages (at 0 pages/min), scraped 0 items (at 0 items/min)
2018-07-03 10:59:04 [scrapy.extensions.telnet] DEBUG: Telnet console listening on 127.0.0.1:6024
2018-07-03 10:59:04 [scrapy.downloadermiddlewares.retry] DEBUG: Retrying <GET file:///robots.txt> (failed 1 times): [Errno 2] No such file or directory: '/robots.txt'
2018-07-03 10:59:04 [scrapy.downloadermiddlewares.retry] DEBUG: Retrying <GET file:///robots.txt> (failed 2 times): [Errno 2] No such file or directory: '/robots.txt'
2018-07-03 10:59:04 [scrapy.downloadermiddlewares.retry] DEBUG: Gave up retrying <GET file:///robots.txt> (failed 3 times): [Errno 2] No such file or directory: '/robots.txt'
2018-07-03 10:59:04 [scrapy.downloadermiddlewares.robotstxt] ERROR: Error downloading <GET file:///robots.txt>: [Errno 2] No such file or directory: '/robots.txt'
Traceback (most recent call last):
  File "/home/xiong/Python3.7/lib/python3.7/site-packages/twisted/internet/defer.py", line 150, in maybeDeferred
    result = f(*args, **kw)
  File "/home/xiong/Python3.7/lib/python3.7/site-packages/scrapy/core/downloader/handlers/file.py", line 13, in download_request
    with open(filepath, 'rb') as fo:
FileNotFoundError: [Errno 2] No such file or directory: '/robots.txt'
2018-07-03 10:59:04 [scrapy.core.engine] DEBUG: Crawled (200) <GET file:///home/xiong/learn-scrapy/web/20.html> (referer: None)
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['Lough'],
 'link': 'file:///home/xiong/learn-scrapy/web/blue-glasses.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t防蓝光眼睛也许没你想得那么厉害。\t'],
 'title': ['防蓝光眼镜真的有用吗？']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['蛋花'],
 'link': 'file:///home/xiong/learn-scrapy/web/dr-doolittle.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t她抬头看到一只狐猴正盯着她看，然后在她头上拉了一泡屎。\t'],
 'title': ['灵媒还是骗子？她说自己能和动物交流']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['HW'],
 'link': 'file:///home/xiong/learn-scrapy/web/narcissism-bias.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t其实大家都在看自己的自拍照。\t'],
 'title': ['你的自拍没有你想的那样吸引人']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['majer'],
 'link': 'file:///home/xiong/learn-scrapy/web/new-food-trend.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t大多数澳洲人每日蔬菜摄入不足。\t'],
 'title': ['想来一杯西兰花味的健康咖啡么']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['Lough'],
 'link': 'file:///home/xiong/learn-scrapy/web/coffee-drinker.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t一千个喝咖啡的人，就有一千种对咖啡的反应。\t'],
 'title': ['每个人对咖啡的反应取决于基因']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['蛋花'],
 'link': 'file:///home/xiong/learn-scrapy/web/machine-painter.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t十九年前，父亲突然去世，他被迫辍学经营家族生意——裁缝。\t'],
 'title': ['针线画家：独一无二的缝纫机艺术家']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['蛋花'],
 'link': 'file:///home/xiong/learn-scrapy/web/whose-wife.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t好基友，一辈子\t'],
 'title': ['老婆不让去俄罗斯看世界杯怎么办？']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['majer'],
 'link': 'file:///home/xiong/learn-scrapy/web/gaming-isorder.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t可以合法地将游戏沉迷当作精神疾患治疗了\t'],
 'title': ['游戏成瘾或将被世卫组织列入精神疾病']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['majer'],
 'link': 'file:///home/xiong/learn-scrapy/web/man-made-diamonds.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t钻石恒久远,一颗永留传。公司高层曾发誓说永远不会销售没有灵魂的人造珠宝\t'],
 'title': ['戴比尔斯将发售人造钻石']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['dubulidudu'],
 'link': 'file:///home/xiong/learn-scrapy/web/sd-me-manta-ray.html',
 'summary': ['\r\n\t',
             '\r\n',
             '\r\n\t蝠鲼通常需要4-5年才能成年，而幼年蝠鲼却极为罕见——原来它们都躲在“育儿园”里。\t'],
 'title': ['科学家首次发现海洋中的蝠鲼“育儿园”']}
2018-07-03 10:59:04 [scrapy.core.scraper] DEBUG: Scraped from <200 file:///home/xiong/learn-scrapy/web/20.html>
{'author': ['Imagine'],
 'link': 'file:///home/xiong/learn-scrapy/web/low-self-esteem.html',
 'summary': ['\r\n\t', '\r\n', '\r\n\t今天，你又因为嫉妒心冲动消费了吗？\t'],
 'title': ['自信心决定了嫉妒营销的成功与否']}
2018-07-03 10:59:04 [scrapy.core.engine] INFO: Closing spider (finished)
2018-07-03 10:59:04 [scrapy.statscollectors] INFO: Dumping Scrapy stats:
{'downloader/exception_count': 3,
 'downloader/exception_type_count/builtins.FileNotFoundError': 3,
 'downloader/request_bytes': 861,
 'downloader/request_count': 4,
 'downloader/request_method_count/GET': 4,
 'downloader/response_bytes': 14720,
 'downloader/response_count': 1,
 'downloader/response_status_count/200': 1,
 'finish_reason': 'finished',
 'finish_time': datetime.datetime(2018, 7, 3, 2, 59, 4, 771518),
 'item_scraped_count': 11,
 'log_count/DEBUG': 16,
 'log_count/ERROR': 1,
 'log_count/INFO': 7,
 'memusage/max': 46104576,
 'memusage/startup': 46104576,
 'response_received_count': 1,
 'retry/count': 2,
 'retry/max_reached': 1,
 'retry/reason_count/builtins.FileNotFoundError': 2,
 'scheduler/dequeued': 1,
 'scheduler/dequeued/memory': 1,
 'scheduler/enqueued': 1,
 'scheduler/enqueued/memory': 1,
 'start_time': datetime.datetime(2018, 7, 3, 2, 59, 4, 614370)}
2018-07-03 10:59:04 [scrapy.core.engine] INFO: Spider closed (finished)
```


## 1.1.1 编写第一个爬虫
spider是用户编写用于从单个完善或者一些网站爬取数据的类.

其包含了一个用于下载的初始URL, 如何跟进网页中的链接, 以及如何分析页面中的内容, 提取>生成 item的方法.

为了创建一个Spider, 你必须继承`scrapy.Spider`类, 且定义一些属性:
- `name` : 用于区别Spider. 该名字必须是唯一的, 你不可以为不同的Spider设定相同的名字.
- `start_urls` : 包含了Spider在启动时进行爬取的url列表. 因此, 第一个被获取到的页面将
是其中之一. 后续的url则从初始的url获取到的数据中提取.
- `parse()` : 是Spider的一个方法. 被调用时, 每个初始URL完成下载后生成的`Response`对象将会作为唯一的参数传递给该函数. 该方法负责解析返回的数据(response data), 提取数据(生成item)以及生成需要进一步处理的URL的`Request`对象.

以下为我们的第一个Spider代码, 保存在`jandan/spiders/jdSpider.py`文件中:
```
# -*- coding:utf8 -*-
import scrapy

class jiandSpider(scrapy.Spider):
    name = 'jand'
    allow_domains = ['jandan.net']
    start_urls = [
        'file:///home/xiong/learn-scrapy/web/20.html',
    ]
    def parse(self, response):
        filename = response.url.split("/")[-1] + '.html'
        with open(filename, 'wb') as f:
            f.write(response.body)
```
#### 爬取 
进入项目根目录, 执行下列命令启动spider:
```
scrapy crawl jand
```
该命令启动了, 我们刚刚添加的`jand`spider, 向目标地址发送一些请求. 你将会得到类似的输出(关于reboot.txt的错误忽略):
```
scrapy crawl jand
2018-06-29 14:44:33 [scrapy.utils.log] INFO: Scrapy 1.5.0 started (bot: jandan)
2018-06-29 14:44:33 [scrapy.utils.log] INFO: Versions: lxml 4.2.2.0, libxml2 2.9.8, cssselect 1.0.3, parsel 1.4.0, w3lib 1.19.0, Twisted 18.4.0, Python 3.7.0a3 (default, Apr 22 2018, 16:54:46) - [GCC 4.4.7 20120313 (Red Hat 4.4.7-18)], pyOpenSSL 18.0.0 (OpenSSL 1.1.0h  27 Mar 2018), cryptography 2.2.2, Platform Linux-2.6.32-573.el6.x86_64-x86_64-with-centos-6.7-Final
2018-06-29 14:44:33 [scrapy.crawler] INFO: Overridden settings: {'BOT_NAME': 'jandan', 'NEWSPIDER_MODULE': 'jandan.spiders', 'ROBOTSTXT_OBEY': True, 'SPIDER_MODULES': ['jandan.spiders']}
2018-06-29 14:44:33 [scrapy.middleware] INFO: Enabled extensions:
['scrapy.extensions.corestats.CoreStats',
 'scrapy.extensions.telnet.TelnetConsole',
 'scrapy.extensions.memusage.MemoryUsage',
 'scrapy.extensions.logstats.LogStats']
2018-06-29 14:44:33 [scrapy.middleware] INFO: Enabled downloader middlewares:
['scrapy.downloadermiddlewares.robotstxt.RobotsTxtMiddleware',
 'scrapy.downloadermiddlewares.httpauth.HttpAuthMiddleware',
 'scrapy.downloadermiddlewares.downloadtimeout.DownloadTimeoutMiddleware',
 'scrapy.downloadermiddlewares.defaultheaders.DefaultHeadersMiddleware',
 'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware',
 'scrapy.downloadermiddlewares.retry.RetryMiddleware',
 'scrapy.downloadermiddlewares.redirect.MetaRefreshMiddleware',
 'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware',
 'scrapy.downloadermiddlewares.redirect.RedirectMiddleware',
 'scrapy.downloadermiddlewares.cookies.CookiesMiddleware',
 'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware',
 'scrapy.downloadermiddlewares.stats.DownloaderStats']
2018-06-29 14:44:33 [scrapy.middleware] INFO: Enabled spider middlewares:
['scrapy.spidermiddlewares.httperror.HttpErrorMiddleware',
 'scrapy.spidermiddlewares.offsite.OffsiteMiddleware',
 'scrapy.spidermiddlewares.referer.RefererMiddleware',
 'scrapy.spidermiddlewares.urllength.UrlLengthMiddleware',
 'scrapy.spidermiddlewares.depth.DepthMiddleware']
2018-06-29 14:44:33 [scrapy.middleware] INFO: Enabled item pipelines:
[]
2018-06-29 14:44:33 [scrapy.core.engine] INFO: Spider opened
2018-06-29 14:44:33 [scrapy.extensions.logstats] INFO: Crawled 0 pages (at 0 pages/min), scraped 0 items (at 0 items/min)
2018-06-29 14:44:33 [scrapy.extensions.telnet] DEBUG: Telnet console listening on 127.0.0.1:6024
2018-06-29 14:44:33 [scrapy.downloadermiddlewares.retry] DEBUG: Retrying <GET file:///robots.txt> (failed 1 times): [Errno 2] No such file or directory: '/robots.txt'
2018-06-29 14:44:33 [scrapy.downloadermiddlewares.retry] DEBUG: Retrying <GET file:///robots.txt> (failed 2 times): [Errno 2] No such file or directory: '/robots.txt'
2018-06-29 14:44:33 [scrapy.downloadermiddlewares.retry] DEBUG: Gave up retrying <GET file:///robots.txt> (failed 3 times): [Errno 2] No such file or directory: '/robots.txt'
2018-06-29 14:44:33 [scrapy.downloadermiddlewares.robotstxt] ERROR: Error downloading <GET file:///robots.txt>: [Errno 2] No such file or directory: '/robots.txt'
Traceback (most recent call last):
  File "/home/xiong/Python3.7/lib/python3.7/site-packages/twisted/internet/defer.py", line 150, in maybeDeferred
    result = f(*args, **kw)
  File "/home/xiong/Python3.7/lib/python3.7/site-packages/scrapy/core/downloader/handlers/file.py", line 13, in download_request
    with open(filepath, 'rb') as fo:
FileNotFoundError: [Errno 2] No such file or directory: '/robots.txt'
2018-06-29 14:44:33 [scrapy.core.engine] DEBUG: Crawled (200) <GET file:///home/xiong/learn-scrapy/web/20.html> (referer: None)
2018-06-29 14:44:33 [scrapy.core.engine] INFO: Closing spider (finished)
2018-06-29 14:44:33 [scrapy.statscollectors] INFO: Dumping Scrapy stats:
{'downloader/exception_count': 3,
 'downloader/exception_type_count/builtins.FileNotFoundError': 3,
 'downloader/request_bytes': 861,
 'downloader/request_count': 4,
 'downloader/request_method_count/GET': 4,
 'downloader/response_bytes': 30655,
 'downloader/response_count': 1,
 'downloader/response_status_count/200': 1,
 'finish_reason': 'finished',
 'finish_time': datetime.datetime(2018, 6, 29, 6, 44, 33, 902984),
 'log_count/DEBUG': 5,
 'log_count/ERROR': 1,
 'log_count/INFO': 7,
 'memusage/max': 43769856,
 'memusage/startup': 43769856,
 'response_received_count': 1,
 'retry/count': 2,
 'retry/max_reached': 1,
 'retry/reason_count/builtins.FileNotFoundError': 2,
 'scheduler/dequeued': 1,
 'scheduler/dequeued/memory': 1,
 'scheduler/enqueued': 1,
 'scheduler/enqueued/memory': 1,
 'start_time': datetime.datetime(2018, 6, 29, 6, 44, 33, 755732)}
2018-06-29 14:44:33 [scrapy.core.engine] INFO: Spider closed (finished)
```
---
**注解**

最后你可以看到有一行log包含定义在`start_urls`的初始url, 并且与spider中是一一对应的. 在log中可以看到其没有指向其他页面((referer: None)).
---
现在, 查看当前目录, 你将会注意到有两个包含url所对应的内容的文件被创建了: 20.html 正如我们的`parse`方法
里做的一样.

### 刚才发生了什么?
scrapy 为 spider 的`start_urls`属性中的每个URL创建了`scrapy.request`对象, 并将`parse`方法为回调函数(callback)赋
值给了Request.

Request对象经过调度, 执行生成`scrapy.http.Response`对象并送回给spider`parse()`方法.


## 1.1.0 Scrapy入门
选择一个网站:
```
file:///home/xiong/learn-scrapy/web/20.html
```
本篇教程中将带你完成下列任务:
1. 创建一个Scrapy项目;
2. 编写爬取网站的 spider ;
3. 定义提取Item, 并编写 Item Pipline 来存储提取到的 Item;

## 创建项目
开始爬取之前, 必须创建一个新的Scrapy项目. 进入打算存储代码的目录, 运行以下命令:
```
[xiong@localhost learn-scrapy]$ scrapy startproject jandan
New Scrapy project 'jandan', using template directory '/home/xiong/Python3.7/lib/python3.7/site-packages/scrapy/templates/project', created in:
    /home/xiong/learn-scrapy/jandan

You can start your first spider with:
    cd jandan
    scrapy genspider example example.com
```
该命令将会创建以下内容的`jandan`目录
```
jandan/
├── jandan
│   ├── __init__.py
│   ├── items.py
│   ├── middlewares.py
│   ├── pipelines.py
│   ├── settings.py
│   └── spiders
│       └── __init__.py
└── scrapy.cfg
```
